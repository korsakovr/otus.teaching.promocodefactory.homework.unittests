﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerPromoCodeLimitBuilder
    {
        private int _limit;
        private DateTime? _cancelDate;

        public PartnerPromoCodeLimitBuilder WithCreatedLimit(int limit)
        {
            this._limit = limit;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithCreatedCancelDate(DateTime? cancelDate)
        {
            this._cancelDate = cancelDate;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit()
            {
                Limit = _limit,
                CancelDate = _cancelDate
            };
        }
    }
}
