﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Fixures;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitInMemoryAsyncTests : IClassFixture<FixureInMemory>
    {
        private readonly PartnersController _partnersController;
        private readonly IRepository<Partner> _inMemoryPartnerRepository;

        public SetPartnerPromoCodeLimitInMemoryAsyncTests(FixureInMemory fixureInMemory)
        {
            _partnersController = fixureInMemory.ServiceProvider.GetService<PartnersController>();
            _inMemoryPartnerRepository = fixureInMemory.ServiceProvider.GetService<IRepository<Partner>>();
        }

        [Fact]
        // Убедиться, что сохранили новый лимит в базу данных 
        public async void SetPartnerPromoCodeLimit_AddLimit_LimitSavedInDb()
        {

            // Arrange

            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = await _inMemoryPartnerRepository.GetByIdAsync(partnerId);
            var request = CreateBaseSetPartnerPromoCodeLimitRequest(4);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var limitIdValue = (result as CreatedAtActionResult)?.RouteValues["limitId"];
            var limitId = limitIdValue is Guid ? (Guid)limitIdValue : new Guid();
            var limit = partner.PartnerLimits.SingleOrDefault(x => x.Id == limitId);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            limit.Should().NotBeNull();
            
        }

        private SetPartnerPromoCodeLimitRequest CreateBaseSetPartnerPromoCodeLimitRequest(int limit)
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today,
                Limit = limit
            };
        }
    }
}
