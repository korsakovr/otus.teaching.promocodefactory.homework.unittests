﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private bool _isActive;
        private Guid _id;
        private List<PartnerPromoCodeLimit> _promocodeLimits;
        private int _numberIssuedPromoCodes;
        public PartnerBuilder()
        {
            this._promocodeLimits = new List<PartnerPromoCodeLimit>();
        }

        public PartnerBuilder WithCreatedId(Guid id)
        {
            this._id = id;
            return this;
        }

        public PartnerBuilder WithCreatedIsActive(bool isActive)
        {
            this._isActive = isActive;
            return this;
        }

        public PartnerBuilder WithCreatedNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            this._numberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public PartnerBuilder AddLimit(PartnerPromoCodeLimit promocodeLimit)
        {
            _promocodeLimits.Add(promocodeLimit);
            return this;
        }

        public Partner Build()
        {
            return new Partner()
            {
                Id = _id,
                IsActive = _isActive,
                PartnerLimits = _promocodeLimits,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes
            };
        }
    }
}
