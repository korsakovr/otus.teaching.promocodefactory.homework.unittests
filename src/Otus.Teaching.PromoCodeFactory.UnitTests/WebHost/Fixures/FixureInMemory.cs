﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Fixures
{
    public class FixureInMemory
    {
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }
        public FixureInMemory()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            this.ServiceCollection = Configurator.GetServiceCollection(configuration, null);
            this.ServiceCollection.AddTransient<PartnersController>();
            this.ServiceProvider = GetServiceProvider();
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();

            SeedData(serviceProvider);
            return serviceProvider;
        }

        private void SeedData(IServiceProvider serviceProvider)
        {
            var initializer = serviceProvider.GetService<IDbInitializer>();
            initializer.InitializeDb();
        }


    }
}
